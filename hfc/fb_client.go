package hfc

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	rateLimitCode = 32
	rateLimitErr  = "RATE_LIMIT"
	baseUrl       = "https://graph.facebook.com/v4.0/"
)

type Comments struct {
	Data []struct {
		ID       string `json:"id"`
		IsHidden bool   `json:"is_hidden"`
	} `json:"data"`
	Error FBError `json:"error"`
}
type Response struct {
	Success bool    `json:"success"`
	Error   FBError `json:"error"`
}
type FBError struct {
	Message      string `json:"message"`
	Type         string `json:"type"`
	Code         int    `json:"code"`
	ErrorSubcode int    `json:"error_subcode"`
	FbtraceID    string `json:"fbtrace_id"`
}

func ProcessPost(pageId, postId, accessToken string) {
	comments, err := getComments(pageId, postId, accessToken)
	if err != nil {
		handleApiError(err)
	}
	for _, postCommentId := range comments.Data {
		if !postCommentId.IsHidden {
			err = hideComment(postCommentId.ID, accessToken)
			if err != nil {
				handleApiError(err)
			}
		}
	}
}

func getComments(pageId string, postId string, token string) (*Comments, error) {

	theUrl := fmt.Sprintf(baseUrl+"%s_%s/comments?fields=id,is_hidden&limit=5000&access_token=%s",
		pageId, postId, token)
	resp, err := http.Get(theUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var comments Comments
	body, _ := ioutil.ReadAll(resp.Body)
	_ = json.Unmarshal(body, &comments)

	return &comments, buildApiError(comments.Error)
}

func hideComment(postCommentId string, token string) error {
	fmt.Printf("Hiding comment with postCommentId: %s\n", postCommentId)

	theUrl := fmt.Sprintf(baseUrl+"%s?access_token=%s&is_hidden=true", postCommentId, token)
	resp, err := http.Post(theUrl, "", nil)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	var response Response
	body, _ := ioutil.ReadAll(resp.Body)
	_ = json.Unmarshal(body, &response)

	return buildApiError(response.Error)
}

func buildApiError(fbError FBError) error {
	if fbError.Code != 0 {
		if fbError.Code == rateLimitCode {
			return errors.New(rateLimitErr)
		} else if fbError.Code == 1 && fbError.ErrorSubcode == 1446036 {
			// do nothing, Comment is already hidden
		} else {
			return errors.New(fmt.Sprintf("Error: (type: %s, code: %d, subCode: %d, message: %s)",
				fbError.Type, fbError.Code, fbError.ErrorSubcode, fbError.Message))
		}
	}
	return nil
}

func handleApiError(err error) {
	if err.Error() == rateLimitErr {
		fmt.Println("Rate limiting reached, Sleeping for 10 minutes...")
		time.Sleep(10 * time.Minute)
	} else {
		fmt.Println(err.Error())
	}
}
