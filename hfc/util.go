package hfc

import (
	"fmt"
	"os"
)

func exit(format string, a ...interface{}) {
	if a != nil {
		fmt.Printf(format+"\n", a...)
	} else {
		fmt.Println(format)
	}
	os.Exit(1)
}
