package hfc

import (
	"flag"
	"io/ioutil"
	"os"
	"strings"
)

func ParseArgs() (string, string) {
	config := flag.String("config", "", "location of config file")
	locate := flag.String("locate", "", "location of posts file")
	flag.Parse()

	if len(*config) == 0 || len(*locate) == 0 {
		flag.Usage()
		os.Exit(2)
	}
	return *config, *locate
}

func ReadConfig(config string) (pageId string, token string) {

	props, err := ioutil.ReadFile(config)
	if err != nil {
		exit("File %s not found ", config)
	}
	lines := strings.Split(string(props), "\n")
	for _, line := range lines {
		arr := strings.Split(line, "=")
		if arr[0] == "pageId" {
			pageId = arr[1]
			continue
		}
		if arr[0] == "token" {
			token = arr[1]
			continue
		}
	}
	if len(pageId) == 0 {
		exit("Missing \"pageId\" parameter in file %s", config)
	}
	if len(token) == 0 {
		exit("Missing \"token\" parameter in file  %s", config)
	}
	return pageId, token
}

func ReadPostIds(locate string) []string {

	props, err := ioutil.ReadFile(locate)
	if err != nil {
		exit("File %s not found", locate)
	}
	return strings.Split(string(props), "\n")
}
