package main

import "gitlab.com/mhewedy/hide-fb-comments/hfc"

func main() {

	config, locate := hfc.ParseArgs()
	pageId, token := hfc.ReadConfig(config)

	for _, postId := range hfc.ReadPostIds(locate) {
		if len(postId) > 0 {
			hfc.ProcessPost(pageId, postId, token)
		}
	}
}
