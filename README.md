## Usage

1. create a configuration file as follows:
```properties
pageId=<facebook page id>
token=<facebook access token>
```
2. create a posts file that contains ids as:
```txt
first_post_id
second_post_id
third_post_id
....
```
3. execute the command without any parameters:
```bash
./hide_fb_comments -locate /path/to/posts/file -config /path/to/config/file
``` 
## Download
[Download Linux executable](https://drive.google.com/open?id=1L-LS4xlUyrL3IjRrQu9Mol085wPO6Dwl)